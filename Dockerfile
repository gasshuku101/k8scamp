FROM golang:1.16.5-alpine3.12 AS builder

WORKDIR /build 

COPY . .

RUN go mod download 

RUN go version

RUN go build -o appbin hello-world/app

WORKDIR /dist 

RUN cp /build/appbin .

FROM alpine:latest

WORKDIR /app

COPY --from=builder /dist/appbin .

RUN mkdir -p /app/tmp-images

expose 80

CMD ["/app/appbin"]

#reference https://tutorialedge.net/golang/go-docker-tutorial/