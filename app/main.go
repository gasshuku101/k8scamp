package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gorilla/mux"
)

func main() {
	defer closer()
	r := mux.NewRouter()
	r.HandleFunc("/", helloWorldHandler)
	server := &http.Server{
		Handler: r,
		Addr:    ":80",
	}
	quitChannel := make(chan os.Signal)
	signal.Notify(quitChannel, syscall.SIGINT, syscall.SIGTERM)

	go server.ListenAndServe()

	select {
	case <-quitChannel:
		ctxShutDown, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer func() {
			cancel()
		}()
		server.Shutdown(ctxShutDown)
	}

}

func closer() {
	fmt.Println("bye!")
}

func helloWorldHandler(w http.ResponseWriter, r *http.Request) {
	hn, _ := os.Hostname()
	msg := fmt.Sprintf("hello, world from %v!\n", hn)
	w.Write([]byte(msg))

}
