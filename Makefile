dockerbuild:
	@docker build -t k8s-tutorial/go-hello-world .

runcontainer: dockerbuild
	docker run --publish 80:80 k8s-tutorial/go-hello-world:latest

minikube-dockerbuild:
	@eval $(minikube docker-env)
	@docker build -t k8s-tutorial/go-hello-world .