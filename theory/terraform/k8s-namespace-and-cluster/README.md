# Provisioning Kubernetes Namespace and Deployment #

## Set up minikube cluster ##
setup the minikube cluster. see [here](../../00.installation.md) and see [here](../../01.start-minikube.md)

## .tf config files ##
### providers.tf ###
refer to the [providers.tf](providers.tf) file.

### k8s.tf ###


## Applying the changes #
## init ##

```
> terraform init
```

## plan ##
```
> terraform plan
```

## apply ##
```
> terraform apply
```

## destroy ##
```
> terraform destroy
```